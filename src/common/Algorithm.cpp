#include "example-subsystem/swatch/Algorithm.hpp"


namespace examplesubsystem {
namespace swatch {

using namespace ::swatch;

Algorithm::Algorithm() :
  AlgoInterface()
// FIXME: Register metrics for monitoring data here
{
}


Algorithm::~Algorithm()
{
}


void Algorithm::retrieveMetricValues()
{
  // FIXME: Read monitoring data, and set values of corresponding metrics (in long term)
}

} // namespace swatch
} // namespace examplesubsystem
