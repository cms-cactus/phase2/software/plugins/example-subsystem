#ifndef __EXAMPLE_SUBSYSTEM_SWATCH_PROCESSOR_HPP__
#define __EXAMPLE_SUBSYSTEM_SWATCH_PROCESSOR_HPP__

#include "exampleboard/swatch/Processor.hpp"


namespace examplesubsystem {
namespace swatch {

// Represents a data-processing FPGA that implements the main function of a board (e.g. the trigger algorithm)
class Processor : public exampleboard::swatch::Processor {
public:
  Processor(const ::swatch::core::AbstractStub& aStub);
  ~Processor();
};

} // namespace swatch
} // namespace examplesubsystem


#endif